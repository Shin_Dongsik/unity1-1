﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoginControler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Screen.SetResolution(640, 480, true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //게임씬 로딩
    public void LoadGameScene()
    {
        //SceneManager.LoadScene("Game");     //게임씬로딩
        SceneManager.LoadScene("Title");  //타이틀씬 로딩  
    }
}
